package test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

import model.Card;
import model.Student;
import gui.SoftwareFrame;

public class SoftwareTest {

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new SoftwareTest();
	}

	public SoftwareTest() {
		frame = new SoftwareFrame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(400, 400);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		/*
		 * Student may modify this code and write your result here
		 * frame.setResult("aaa\t bbb\t ccc\t"); frame.extendResult("ddd");
		 */
		Card card = new Card(0);
		
		frame.setResult("balance" + card.toString());
		int amount = 1000;
		card.deposit(amount);
		frame.extendResult("deposit" + amount);
		card.getBalance();
		frame.extendResult("balance" + card.toString());
		int amount1 = 200;
		card.withdraw(amount1);
		frame.extendResult("withdraw" + amount1);
		card.getBalance();
		frame.extendResult("balance" + card.toString());
		
		

	}

	ActionListener list;
	SoftwareFrame frame;
}
